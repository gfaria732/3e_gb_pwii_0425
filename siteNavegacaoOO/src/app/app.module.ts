import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { CadImobiliariaComponent } from './components/cad-imobiliaria/cad-imobiliaria.component';
import { CadProprietarioComponent } from './components/cad-proprietario/cad-proprietario.component';
import { CadLocadorComponent } from './components/cad-locador/cad-locador.component';

@NgModule({
  declarations: [
    AppComponent,
    CadImovelComponent,
    CadImobiliariaComponent,
    CadProprietarioComponent,
    CadLocadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
